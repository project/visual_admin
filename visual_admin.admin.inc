<?php

/**
 * @file .admin.inc
 * Functions that are only called on the admin pages.
 */

/******************************************************************************
 *                   OVERVIEW                                                 *
 ******************************************************************************/

/**
 * Page showing the entire list of buttons.
 */
function visual_admin($type = 'new') {
	$output="";

	$output.=drupal_get_form('visual_admin_overview',$type);
	/*$output.=drupal_get_form('visual_admin_edit_group',"add");
	$output.=drupal_get_form('visual_admin_edit_button',"add");*/

	return $output;

}

/**
 * Form builder; Builds the visual_admin overview form for the admin.
 *
 * @param $type
 *   Not used.
 * @return
 *   The form structure.
 * @ingroup forms
 * @see visual_admin_admin_overview_validate()
 * @see visual_admin_admin_overview_submit()
 * @see theme_visual_admin_admin_overview()
 */
function visual_admin_overview($type = 'new') {

	// build an 'Update options' form
	$form['options'] = array(
    '#type' => 'fieldset', '#title' => t('Update options'),
    '#prefix' => '<div class="container-inline">', '#suffix' => '</div>'
    );
    $options = array();
    foreach (visual_admin_operations() as $operation => $array) {
    	$options[$operation] = $array['label'];
    }
    $form['options']['operation'] = array('#type' => 'select', '#options' => $options, '#default_value' => 'publish');
    
    // load the visual_admins that we want to display
    $form['header'] = array('#type' => 'value', '#value' => array(
    array('data' => t('Title'), 'field' => 'title'),
    array('data' => t('Path'), 'field' => 'path'),
    array('data' => t('Image'), 'field' => 'image'),
    array('data' => t('Weight'), 'field' => 'weight'),
    array('data' => t('Group'), 'field' => 'region'),
    theme('table_select_header_cell'),
    ));
    
    // build a table listing the appropriate buttons&groups
    $groups_list = visual_admin_list_groups(0);
    $group_none=new StdClass();    // we add the "None" group element
    $group_none->gid=0;
    $groups_list=array("-1"=>$group_none)+$groups_list;
    
    // we fill the form items
    $buttons = array();
    $form['items'] = array('#tree' => TRUE);
    $count=0;
    foreach ($groups_list as $group) {
    	// we create a group entry
    	if ($group->gid!=0){
    		$id=$count++;
    		$buttons[$id] = '';
    		$form['items'][$id]=visual_admin_group_entry($group);
    	}

    	// then we create buttons entries
    	$buttons_list = visual_admin_list_buttons($group->gid);
    	foreach ($buttons_list as $button) {
    		$id=$count++;
    		$buttons[$id] = '';
    		$form['items'][$id]=visual_admin_button_entry($button);    		
    	}
    }
    
    $form['count'] = array('#type' => 'hidden','#value' => $count);
    $form['buttons'] = array('#type' => 'checkboxes', '#options' => isset($buttons) ? $buttons: array());
    $form['pager'] = array('#value' => theme('pager', NULL, 50, 0));
    $form['submit'] = array('#type' => 'submit','#value' => t('Update'),'#action' => 'visual_admin_overview_submit');

    return $form;
}

/**
 * Process visual_admin_overview form submissions.
 *
 * Execute the chosen 'Update option' on the selected buttons, such as
 * publishing, unpublishing or deleting.
 */
function visual_admin_overview_submit($form, &$form_state) {
	$operations = visual_admin_operations();
	$operation = $operations[$form_state['values']['operation']];
	// Filter out unchecked buttons
	$buttons = array_filter($form_state['values']['buttons']);
	if ($function = $operation['callback']) {
		// Add in callback arguments if present.
		if (isset($operation['callback arguments'])) {
			$args = array_merge(array($buttons), $operation['callback arguments']);
		}
		else {
			$args = array($buttons);
		}
		call_user_func_array($function, $args);
	}
	else {
		// we need to rebuild the form to go to a second step.  For example, to
		// show the confirmation form for the deletion of nodes.
		$form_state['rebuild'] = TRUE;
	}
	// we save individual element weights
	foreach ($form['items'] as $id => $item) {
		if ($item['type']['#value']=="button"&&($item['weight']['#value']!=$item['weight']['#default_value'])){
			visual_admin_update_button_weight($item['db_id']['#value'],$item['weight']['#value']);
		}
		if ($item['type']['#value']=="group"&&($item['weight']['#value']!=$item['weight']['#default_value'])) {
			visual_admin_update_group_weight($id,$item['weight']['#value']);
		}
	}
	drupal_set_message(t('The elements ordering has been saved.'));
}

/**
 * Create the entry corresponding to a button.
 *
 * @param $button the button to create an entry corresponding to
 * @return array prepared to fill in the overview form
 */
function visual_admin_button_entry($button) {
	$entry=array();

	$entry['title'] = array('#value' => l($button->title, 'admin/build/visual_admin/button/edit/'. $button->bid, array('attributes' => array('title' => t("Edit button ").$button->title))));
	$entry['path'] = array('#value' => $button->path);
	$entry['weight'] = array('#type' => 'weight', '#default_value' => $button->weight);
	$indentation=($button->gid!=0)? 1:0;
	$entry['indentation'] = array('#type' => 'hidden','#value' => $indentation);
	$entry['type'] = array('#type' => 'hidden','#value' =>'button');
	$entry['region'] = array('#type' => 'select','#default_value' => 'region-'.$button->gid,'#options' =>visual_admin_group_select());
	$entry['db_id'] = array('#type' => 'hidden','#value' =>$button->bid);
	
	return $entry;

}

/**
 * Create the entry corresponding to a group.
 *
 * @param $group the group to create an entry corresponding to
 * @return array prepared to fill in the overview form
 */
function visual_admin_group_entry($group) {
	$entry=array();

	$entry['title'] = array('#value' => l($group->title, 'admin/build/visual_admin/group/edit/'. $group->gid, array('attributes' => array('title' => t("Edit group ").$group->title))));
	$entry['path'] = array('#value' => "");
	$entry['weight'] = array('#type' => 'weight', '#default_value' => $group->weight);
	$entry['type'] = array('#type' => 'hidden','#value' =>'group');
	$entry['region'] = array('#type' => 'hidden','#value' => "region-".$group->gid);
	$entry['db_id'] = array('#type' => 'hidden','#value' =>$group->gid);
	
	return $entry;

}

/******************************************************************************
 *                   SETTINGS                                                 *
 ******************************************************************************/

/**
 * Module settings form.
 */
function visual_admin_settings() {

	// we recreate menu in order to get the proper display page path
	// we have to put this here in order this to be executed AFTER path settings
	// on this page reloading
	// best way would be to have a function fired AFTER settings saving...
	cache_clear_all('*', 'cache_menu', TRUE);
	menu_rebuild();

	$form['general'] = array(
    '#type' => 'fieldset', 
    '#title' => t('General settings'), 
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,	
	);

	$form['general']['visual_admin_title'] = array(
    '#type' => 'textfield',
    '#size' => 20,
    '#title' => t('Title of the visual admin display page'), 
	'#default_value' => variable_get('visual_admin_title', 'Administration'),
	);

	$form['general']['visual_admin_path'] = array(
    '#type' => 'textfield',
    '#size' => 20,
    '#title' => t('Path of the visual admin display page'), 
	'#default_value' => variable_get('visual_admin_path', 'visual_admin/display'),
	);
	
	$form['general']['visual_admin_mandatory_image'] = array(
    '#type' => 'checkbox',
    '#title' => t('Allow buttons without image'), 
    '#description' => t('If checked, buttons without image will be allowed.'), 
	'#default_value' => variable_get('visual_admin_mandatory_image', 0),
	);

	$form['display'] = array(
    '#type' => 'fieldset', 
    '#title' => t('Display settings'), 
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,	
	);
	
	$form['display']['visual_admin_rows'] = array(
    '#type' => 'textfield', 
    '#title' => t('Number of rows'), 
    '#size' => 2,
	'#description' => t('Number of buttons on a same line.'), 
	'#default_value' => variable_get('visual_admin_rows', 4),
	'#valid' => 'integer' ,
	);
	
	$form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
	);

	return system_settings_form($form);
}

/******************************************************************************
 *                   BUTTONS                                                  *
 ******************************************************************************/

/**
 * Edit button form.
 */
function visual_admin_edit_button(&$form_state,$type,$buttonID=NULL) {
	// we change encoding type to load the image
	$form['#attributes'] = array("enctype" => "multipart/form-data");
	
	if ($type != 'add') {
		$button=visual_admin_load_button($buttonID);
	}

	if ($type == 'add' || empty($button)) {
		// This is an add form, initialize the button.
		$button = array('title' => '', 'path' => 0, 'gid' => 0, 'image' => '');
		$form['#op'] = "add";
		$title='Add a new button.';
	}else{
		$form['#op'] = "edit";
		$form['#bid'] = $button->bid;
		$title='Edit a button.';
	}

	$form['button'] = array(
    '#type' => 'fieldset', 
    '#title' => t($title), 
    '#tree' => TRUE,
    '#collapsible' => TRUE,
	);

	$form['button']['title'] = array(
    '#type' => 'textfield',
    '#size' => 10,
    '#title' => t('Title of the new item'), 
    '#tree' => TRUE,
    '#default_value' => $button->title,
	);

	$form['button']['path'] = array(
    '#type' => 'textfield',
    '#size' => 10,
    '#title' => t('Path of the new item'), 
    '#tree' => TRUE,
    '#default_value' => $button->path,
	);

	$form['button']['image'] = array(
        '#type' => 'fieldset',
        '#title' => t('Button Image'),
        '#collapsible' => TRUE,
	);
	if ($image = visual_admin_image_load($buttonID)) {
		$image_display = theme('visual_admin_image_display',$image);
		$form['button']['image']['preview'] = array('#value' => $image_display);
		$form['button']['image']['remove'] = array('#type' => 'checkbox',
        '#title' => t('Remove button image:'),// !image', array('!image' => $image_display)),
        '#weight' => 1,
		);

	}
	$form['button']['image']['image'] = array(
    '#type' => 'file',
	//'#size' => 10,
    '#title' => t('Image associated to the new item'), 
    '#tree' => TRUE,
	// we check that the image is here (if mandatory)
	'#required' => !variable_get('visual_admin_mandatory_image', 0),
	);
	if (!module_exists('imagecache')) {
		$form['button']['image']['image']['#description'] = t('The image will not be resized. Consider installing <a href="@url">Image cache</a>.', array('@url' => url('http://drupal.org/project/imagecache')));
	}

	$form['button']['gid'] = array(
    '#type' => 'select',
    '#options' => visual_admin_group_select(),
    '#title' => t('Parent group of the new item ID'), 
    '#tree' => TRUE,
    '#default_value' => $button->gid,
	);

	$form['button']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
	);

	return $form;
}

/**
 * Form validation.
 */
function visual_admin_edit_button_validate($form, &$form_state) {

	// we check that it is a correct path
	visual_admin_path_validate($form_state['values']['button']['path']);
	
}

/**
 * Button form submission handling.
 */
function visual_admin_edit_button_submit($form, &$form_state) {

	$button=$form_state['values']['button'];
	if ($form['#op']=="add"){
		$button['bid']=visual_admin_insert_button($button);
	}else {
		$button['bid']=$form['#bid'];
		visual_admin_update_button($button);
	}
	// we update the image also
	visual_admin_set_image($button['bid'],$form_state['values']['remove']);

}

/**
 * Image adding
 * @param $button array containing button values
 */
function visual_admin_set_image($bid,$remove=null) {

	$field_name = 'image';
	if ($remove==1) {
		db_query("DELETE FROM {visual_admin_images} WHERE bid = %d", $bid);
	}
	elseif ($file = file_save_upload($field_name)) {
		$file->filepath = str_replace('\\', '/', $file->filepath);
		if (file_copy($file, file_create_path() .'/'. $file->filename)) {
			file_set_status($file, 1);
			if (image_get_info($file->filepath)) {
				//@TODO put it into database file
				db_query("DELETE FROM {visual_admin_images} WHERE bid = %d", $bid);
				db_query("INSERT INTO {visual_admin_images} (fid, bid, filename, filepath, filemime, filesize) VALUES (%d, %d, '%s', '%s', '%s', %d)",
				$file->fid, $bid, $file->filename, $file->filepath, $file->filemime, $file->filesize
				);
			}
			else {
				form_set_error($field_name, t('Uploaded file is not a valid image'));
				file_delete($file->filepath);
			}
		}
	}
}

/******************************************************************************
 *                   GROUPS                                                   *
 ******************************************************************************/

/**
 * Edit group form.
 */
function visual_admin_edit_group(&$form_state,$type,$groupID=NULL) {

	if ($type != 'add') {
		$group=visual_admin_load_group($groupID);
	}

	if ($type == 'add' || empty($group)) {
		// This is an add form, initialize the group.
		$group = array('title' => '', 'path' => 0, 'gid' => 0, 'image' => '');
		$form['#op'] = "add";
		$title='Add a new group.';
	}else{
		$form['#op'] = "edit";
		$form['#gid'] = $group->gid;
		$title='Edit a group.';
	}

	$form['group'] = array(
    '#type' => 'fieldset', 
    '#title' => t($title), 
    '#tree' => TRUE,
    '#collapsible' => TRUE,
	);

	$form['group']['title'] = array(
    '#type' => 'textfield',
    '#size' => 10,
    '#title' => t('Title of the new item'), 
    '#tree' => TRUE,
    '#default_value' => $group->title,
	);

	$form['group']['pid'] = array(
    '#type' => 'select',
    '#options' => visual_admin_group_select($group->gid),
    '#title' => t('Parent group of the new item ID'), 
    '#tree' => TRUE,
    '#default_value' => $group->pid,
	);

	$form['group']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
	);

	return $form;
}

/**
 * group form submission handling.
 */
function visual_admin_edit_group_submit($form, &$form_state) {

	$group=$form_state['values']['group'];
	if ($form['#op']=="add"){
		$group['gid']=visual_admin_insert_group($group);
	}else {
		$group['gid']=$form['#gid'];
		visual_admin_update_group($group);
	}

}

/******************************************************************************
 *                   UTILITIES                                                *
 ******************************************************************************/

/**
 * Provides groups select options.
 * @return array containing groups as gid => title
 */
function visual_admin_group_select($gid=0) {
	$select = array();
	$groups_list = visual_admin_list_groups(0);
	foreach ($groups_list as $group) {
		if ($group->gid!=$gid) $select[$group->gid] = $group->title;
	}
	$select=array("0" => t("--None--"))+$select;
	return $select;
}

/**
 * Validate value for a path being added or edited.
 * inspired from menu_edit_item_validate (menu.admin.inc)
 * @param $path the path to validate
 * @return true if path is ok, false otherwise
 */
function visual_admin_path_validate(&$path) {
	$ret=true;
	$normal_path = drupal_get_normal_path($path);
	if ($path != $normal_path) {
		drupal_set_message(t('The menu system stores system paths only, but will use the URL alias for display. %link_path has been stored as %normal_path', array('%link_path' => $item['link_path'], '%normal_path' => $normal_path)));
		$path = $normal_path;
	}

	if (!menu_path_is_external($path)) {
		$parsed_path = parse_url($path);
		if ($path != $parsed_path['path']) {
			$path = $parsed_path['path'];
		}
	}
	$form_item['link_path']=$path;
	if (!trim($path) || !menu_valid_path($form_item)) {
		form_set_error('link_path', t("The path '@link_path' is either invalid or you do not have access to it.", array('@link_path' => $path)));
		$ret=false;
	}

	return $ret;
}
