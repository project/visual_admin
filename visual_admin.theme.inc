<?php

/**
 * Theme the buttons admin form.
 *
 * @param $form
 *   An associative array containing the structure of the form.
 * @ingroup themeable
 */
function theme_visual_admin_overview($form) {
		
	$rows=array();
	$regions=array();
	$output = drupal_render($form['options']);
	if ($form['count']["#value"]>0) {
		// we loop on every form item (button or group)
		foreach (element_children($form['items']) as $key) {
			// we fill row values in the same order as defined in the header row
			$row = array();
			$row[] = theme('indentation', $form['items'][$key]['indentation']['#value']) . drupal_render($form['items'][$key]['title']);
			$row[] = drupal_render($form['items'][$key]['path']);
			$row[] = theme('visual_admin_image',visual_admin_image_load($form['items'][$key]['db_id']['#value']));
			$form['items'][$key]['weight']['#attributes']['class'] = 'va-element-weight';
			$row[] = drupal_render($form['items'][$key]['weight']);
			$form['items'][$key]['region']['#attributes']['class'] = 'va-element-group';
			$row[] = drupal_render($form['items'][$key]['region']);
			$row[] = drupal_render($form['buttons'][$key]);

			$rows[] = array('data' => $row, 'class' => 'draggable');
			$regions[] = $form['items'][$key]['region']['#value'];
		}
	}
	else {
		$rows[] = array(array('data' => t('No buttons available.'), 'colspan' => '6'));
	}	
	
	$output .= theme('table', $form['header']['#value'], $rows,array('id' => 'va-overview'));
	
	// we add draggable functionnalities, per group
	drupal_add_tabledrag('va-overview', 'order', 'sibling', 'va-element-weight');
	foreach (array_unique($regions) as $region) {
		drupal_add_tabledrag('va-overview', 'match', 'parent', 'va-element-group', 'region-'. $region, NULL, FALSE);
	}		
	
	// we add a pager if needed
	if ($form['pager']['#value']) {
		$output .= drupal_render($form['pager']);
	}

	// let's render
	$output .= drupal_render($form);

	return $output;
}

/**
 * Theme the button image.
 *
 * @param $file
 *   An object representing the image.
 * @ingroup themeable
 */
function theme_visual_admin_image($file,$title="") {

	$imagecache = module_exists('imagecache');
	if ($imagecache) {
		$image_display = theme('imagecache', 'va_120', $file->filepath, t("Go to $title"), t("Click to go to $title"));
	}
	else {
		$image_display = theme('image', $file->filepath, t("Go to $title"), t("Click to go to $title"));
	}
	return $image_display;
}

/**
 * Theme the button display.
 *
 * @param $file
 *   An object representing the image.
 * @ingroup themeable
 */
function theme_visual_admin_button($button) {

	$button_display="";

	$image = visual_admin_image_load($button->bid);
	$button_display.="<div class='visual-admin-button'>";
	$button_display.=l(theme("visual_admin_image",$image,$button->title),$button->path, array('html' => TRUE));
	$button_display.="<br />";
	$button_display.=l(t($button->title),$button->path, array('html' => TRUE));
	$button_display.="</div>";

	return $button_display;
}

/**
 * Theme a group display.
 *
 * @param $groups
 *   A group array.
 * @ingroup themeable
 */
function theme_visual_admin_group($group) {

	$output="";
	$buttons=visual_admin_list_buttons($group->gid);
	if (count($buttons)>0){
		$rows=variable_get('visual_admin_rows', 4);
		$output.="<div class='visual-admin-group'>";
		$output.="<h2>".$group->title."</h2>";
		$row=0;
		foreach ($buttons as $button){
			$row++;
			$output.=theme("visual_admin_button",$button);
			if ($row%$rows==0) $output.="<div class='visual-admin-clear'></div>";
		}
		$output.="<div class='visual-admin-clear'></div>";
		$output.="</div>";
	}

	return $output;
}


/**
 * Theme the general display.
 *
 * @param $buttons
 *   A buttons array.
 * @ingroup themeable
 */
function theme_visual_admin_display($groups) {

	$output="";
	if (count($groups)>0){
		$output.="<div class='visual-admin'>";
		foreach ($groups as $group){
			$output.=theme("visual_admin_group",$group);
		}
		$output.="</div>";
	}

	return $output;
}