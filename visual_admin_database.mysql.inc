<?php

/******************************************************************************
 *                   BUTTONS                                                  *
 ******************************************************************************/

/**
 * @param $bid button ID
 *
 * @return visual_admin button object
 */
function visual_admin_load_button($bid) {

	return db_fetch_object(db_query('SELECT
  bid, gid, title, path,weight FROM {visual_admin_buttons}
  WHERE bid=%s',$bid));
}

/**
 * Insert a new button into the database.
 *
 * @param $button
 *   A full button containing fields from the button form.
 *   @return the ID of the inserted button
 */
function visual_admin_insert_button($button) {
	db_lock_table('visual_admin_buttons');
	db_query("INSERT INTO {visual_admin_buttons} (gid, title, path,weight) VALUES (%d, '%s', '%s', %d)",
	$button['gid'], $button['title'], $button['path'], $button['weight']);
	$bid=mysql_insert_id();
	db_unlock_tables('visual_admin_buttons');
	return $bid;
}

/**
 * Update a button into the database.
 *
 * @param $button
 *   A full button containing fields from the button form.
 */
function visual_admin_update_button($button) {
	db_lock_table('visual_admin_buttons');
	db_query("UPDATE {visual_admin_buttons}
  SET gid=%d,
  title='%s',
  path='%s',
  weight=%d 
  WHERE bid=%d",
	$button['gid'], $button['title'], $button['path'],$button['weight'], $button['bid']);
	db_unlock_tables('visual_admin_buttons');
	return true;
}

/**
 * Update a button weight into the database.
 *
 * @param $bid the button to be updated ID
 * @param $weight the new weight to set
 */
function visual_admin_update_button_weight($bid,$weight) {
	db_lock_table('visual_admin_buttons');
	db_query("UPDATE {visual_admin_buttons}
  SET weight=%d 
  WHERE bid=%d",
	$weight, $bid);
	db_unlock_tables('visual_admin_buttons');
	return true;
}

/**
 * Remove buttons from the database.
 *
 * @param $buttons
 *   A button ids array.
 */
function visual_admin_delete_buttons($buttons) {
	db_lock_table('visual_admin_buttons');
	foreach($buttons as $button){
		db_query("REMOVE FROM {visual_admin_buttons} WHERE bid=%d",$button['bid']);
	}
	db_unlock_tables('visual_admin_buttons');
	return true;
}

/**
 * Publish buttons.
 *
 * @param $buttons
 *   A button ids array.
 */
function visual_admin_publish_buttons($buttons) {
	db_lock_table('visual_admin_buttons');
	foreach($buttons as $bid => $button){
		db_query("UPDATE {visual_admin_buttons} SET status=1 WHERE bid=%d",$bid);
	}
	db_unlock_tables('visual_admin_buttons');
	return true;
}

/**
 * Unpublish buttons.
 *
 * @param $buttons
 *   A button ids array.
 */
function visual_admin_unpublish_buttons($buttons) {
	db_lock_table('visual_admin_buttons');
	foreach($buttons as $bid => $button){
		db_query("UPDATE {visual_admin_buttons} SET status=0 WHERE bid=%d",$bid);
	}
	db_unlock_tables('visual_admin_buttons');
	return true;
}

/**
 * List all buttons of a given group and a given status
 *
 * @param $gid
 *   A group id.
 */
function visual_admin_list_buttons($gid=0) {

	$buttons=array();
	$where="WHERE gid=$gid";
	$order="ORDER BY weight";
	$result=db_query("SELECT  bid,gid,title,path,weight FROM {visual_admin_buttons} $where $order");
	while ($row = db_fetch_object($result)) {
		$buttons[] = $row;
	}

	return $buttons;
}

/******************************************************************************
 *                   GROUPS                                                   *
 ******************************************************************************/

/**
 * @param $gid group ID
 *
 * @return visual_admin group object
 */
function visual_admin_load_group($gid) {

	return db_fetch_object(db_query('SELECT
  gid, pid, title,weight FROM {visual_admin_groups}
  WHERE gid=%s',$gid));
}

/**
 * Insert a new group into the database.
 *
 * @param $group
 *   A full group containing fields from the group form.
 *   @return the ID of the inserted group
 */
function visual_admin_insert_group($group) {
	db_lock_table('visual_admin_groups');
	db_query("INSERT INTO {visual_admin_groups} (pid, title,weight) VALUES (%d, '%s',%d)",
	$group['pid'], $group['title']);
	$gid=mysql_insert_id();
	db_unlock_tables('visual_admin_groups');
	return $gid;
}

/**
 * Update a group into the database.
 *
 * @param $group
 *   A full group containing fields from the group form.
 */
function visual_admin_update_group($group) {
	db_lock_table('visual_admin_groups');
	db_query("UPDATE {visual_admin_groups}
  SET pid=%d,
  title='%s',
  weight=%d 
  WHERE gid=%d",
	$group['pid'], $group['title'],$group['weight'],$group['gid']);
	db_unlock_tables('visual_admin_groups');
	return true;
}

/**
 * Update a group weight into the database.
 *
 * @param $gid the group to be updated ID
 * @param $weight the new weight to set
 */
function visual_admin_update_group_weight($gid,$weight) {
	db_lock_table('visual_admin_groups');
	db_query("UPDATE {visual_admin_groups}
  SET weight=%d 
  WHERE gid=%d",
	$weight, $gid);
	db_unlock_tables('visual_admin_groups');
	return true;
}

/**
 * Remove groups from the database.
 *
 * @param $groups
 *   A group ids array.
 */
function visual_admin_delete_groups($groups) {
	db_lock_table('visual_admin_groups');
	foreach($groups as $group){
		db_query("REMOVE FROM {visual_admin_groups} WHERE gid=%d",$group['gid']);
	}
	db_unlock_tables('visual_admin_groups');
	return true;
}

/**
 * Publish groups.
 *
 * @param $groups
 *   A group ids array.
 */
function visual_admin_publish_groups($groups) {
	db_lock_table('visual_admin_groups');
	foreach($groups as $gid => $group){
		db_query("UPDATE {visual_admin_groups} SET status=1 WHERE gid=%d",$gid);
	}
	db_unlock_tables('visual_admin_groups');
	return true;
}

/**
 * Unpublish groups.
 *
 * @param $groups
 *   A group ids array.
 */
function visual_admin_unpublish_groups($groups) {
	db_lock_table('visual_admin_groups');
	foreach($groups as $gid => $group){
		db_query("UPDATE {visual_admin_groups} SET status=0 WHERE gid=%d",$gid);
	}
	db_unlock_tables('visual_admin_groups');
	return true;
}

/**
 * List all groups of a given group parent and a given status
 *
 * @param $gid
 *   A group id.
 */
function visual_admin_list_groups($pid=0) {

	$groups=array();
	if ($pid!=0) $where="WHERE pid=$pid";
	$order="ORDER BY weight";
	$result=db_query("SELECT  gid,pid,title,weight FROM {visual_admin_groups} $where $order");
	while ($row = db_fetch_object($result)) {
		$groups[] = $row;
	}

	return $groups;
}

/******************************************************************************
 *                   IMAGE                                                    *
 ******************************************************************************/

/**
 * Load a button image.
 */
function visual_admin_image_load($button=0) {
if (is_object($button)) {
    $bid = $button->bid;
  }
  elseif (is_array($button)) {
    $button = $button['bid'];
  }
  else {
    $bid = $button;
  }

  $file = db_fetch_object(db_query("SELECT * FROM {visual_admin_images} WHERE bid = %d", $bid));

  return $file;
}